#ifndef PerfectHashSet_TestFunctions_h
#define PerfectHashSet_TestFunctions_h

#include <iostream>
#include <algorithm>
#include <set>
#include <ctime>

#include "PerfectHashSet.h"
#include "FakePerfectHashSet.h"

namespace NTest {
    
    class CIncorrectWorkException: public NPerfectHashSet::CCommonException {
    public:
        CIncorrectWorkException(unsigned int _key)
        : CCommonException("Mistake in perfect hash set", _key) {}
    };
    
    template<class _Iterator>
    void show(_Iterator _first, _Iterator _second) {
        for (; _first != _second; ++_first)
            std::cout << *_first << " ";
        std::cout << std::endl;
    }
    
    std::vector<unsigned int> generateRandomUI(unsigned int _size, unsigned int _min = 0, unsigned int _max = (unsigned int)1e9) {
        std::vector<unsigned int> result(_size, 0);
        NPerfectHashSet::CRandomNumber<unsigned int> randomNumber(_min, _max);
        std::generate(result.begin(), result.end(), randomNumber);
        return result;
    }
    
    std::vector<unsigned int> generateUniqueUI(unsigned int _size, unsigned int _min = 0, unsigned int _max = (unsigned int)1e9) {
        NPerfectHashSet::CRandomNumber<unsigned int> randomNumber(_min, _max);
        std::set<unsigned int> values;
        while (values.size() < _size) {
            values.insert(randomNumber());
        }
        std::vector<unsigned int> result(values.begin(), values.end());
        std::shuffle(result.begin(), result.end(), std::default_random_engine((unsigned int)time(0)));
        return result;
    }
    
    std::vector<unsigned int> generateTypicalUI(unsigned int _size, unsigned int _type, bool _shuffle, unsigned int _min = 0, unsigned int _max = (unsigned int)1e9) {
        if (_size == 0) return std::vector<unsigned int>();
        std::vector<unsigned int> result(_size);
        std::vector<unsigned int> helper;
        NPerfectHashSet::CRandomNumber<unsigned int> randomNumber(_min, _max), randomIndex(0, _size - 1);
        switch (_type) {
            case 0: // [a, a, a, ...]
                result.assign(_size, randomNumber());
                break;
            case 1: // [a, b, a, b, ...]
                helper = generateUniqueUI(2);
                result[0] = helper[0];
                if (_size > 1) result[1] = helper[1];
                for (unsigned int index = 2; index < _size; ++index)
                    result[index] = result[index % 2];
                break;
            case 2: // [a, a, b, b, c, c, ...]
                helper = generateUniqueUI((_size + 1) / 2);
                for (int index = 0; index < _size; ++index)
                    result[index] = helper[index / 2];
                break;
            case 3: // [a, b, c, ..., z, a]
                helper = generateUniqueUI(_size - 1);
                std::copy(helper.begin(), helper.end(), result.begin());
                result[_size - 1] = result[0];
                break;
            case 4: // [a, b, c, ...] + change random to random
                result = generateUniqueUI(_size);
                helper = generateUniqueUI(2, 0, _size - 1);
                result[helper[0]] = result[helper[1]];
                break;
        }
        if (_shuffle)
            std::shuffle(result.begin(), result.end(), std::default_random_engine((unsigned int)time(0)));
        return result;
    }
    
    enum EMethod {
        IS_POSSIBLE,
        HAS,
        INSERT,
        ERASE,
        TOGGLE
    };
    
    const EMethod fullCheck[] = {HAS, TOGGLE, HAS};
    const unsigned int fullCheckSize = 4;
    
    template<class _HashSet1, class _HashSet2>
    void doAndCheck(EMethod _method, _HashSet1 &_hashSet1, _HashSet2 &_hashSet2, unsigned int _key) {
        bool equal = true;
        switch (_method) {
            case TOGGLE:
                if (_hashSet1.has(_key)) _method = ERASE;
                else _method = INSERT;
            case IS_POSSIBLE:
                equal = _hashSet1.possibleKey(_key) == _hashSet2.possibleKey(_key);
                break;
            case HAS:
                equal = _hashSet1.has(_key) == _hashSet2.has(_key);
                break;
            case INSERT:
                equal = _hashSet1.insert(_key) == _hashSet2.insert(_key);
                break;
            case ERASE:
                equal = _hashSet1.erase(_key) == _hashSet2.erase(_key);
                break;
        }
        if (!equal)
            throw CIncorrectWorkException(_key);
    }
    
    template<class _HashSet1, class _HashSet2>
    void doAndCheckFull(_HashSet1 _hashSet1, _HashSet2 _hashSet2, unsigned int _key) {
        for (unsigned int methodIndex = 0; methodIndex < fullCheckSize; ++methodIndex)
            doAndCheck(fullCheck[methodIndex], _hashSet1, _hashSet2, _key);
    }
    
    void testToggle(const std::vector<unsigned int> &_keys, const std::vector<unsigned int> &_toggles) {
        NPerfectHashSet::CPerfectHashSet PHS;
        PHS.init(_keys);
        CFakePerfectHashSet FPHS;
        FPHS.init(_keys);
        for (const auto &elem: _toggles)
            doAndCheckFull(PHS, FPHS, elem);
    }
    
    void testUnique(unsigned int _tests, unsigned int _keys, unsigned int _queries, unsigned int _min, unsigned int _max) {
        std::vector<unsigned int> keys, toggleIndexes, toggles(_keys);
        for (; _tests > 0; --_tests) {
            keys = generateUniqueUI(_keys, _min, _max);
            toggleIndexes = generateRandomUI(_queries, 0, static_cast<unsigned int>(_keys));
            for (int index = 0; index < _keys; ++index)
                toggles[index] = keys[toggleIndexes[index]];
            testToggle(keys, toggles);
        }
    }
    
    void testPermutations(unsigned int _keys, unsigned int _queries) {
        std::vector<unsigned int> keys(_keys);
        unsigned int index = 0;
        std::generate(keys.begin(), keys.end(), [&] () -> unsigned int { return index++; });
        std::vector<unsigned int> toggles(keys.begin(), keys.end());
        for (; _queries > 0; --_queries) {
            testToggle(keys, toggles);
            std::next_permutation(toggles.begin(), toggles.end());
        }
    }
    
    void testIsPossible(unsigned int _tests, unsigned int _keys, unsigned int _queries, unsigned int _min, unsigned int _max) {
        for (; _tests > 0; --_tests) {
            std::vector<unsigned int> keys = generateUniqueUI(_keys, _min, _max), queries(std::max((unsigned int)keys.size(), _queries));
            std::copy(keys.begin(), keys.end(), queries.begin());
            NPerfectHashSet::CRandomNumber<unsigned int> randomNumber(_min, _max);
            std::generate(queries.begin() + _keys, queries.end(), randomNumber);
            std::shuffle(queries.begin(), queries.end(), std::default_random_engine((unsigned int)time(0)));
            
            NPerfectHashSet::CPerfectHashSet PHS;
            PHS.init(keys);
            CFakePerfectHashSet FPHS;
            FPHS.init(keys);
            
            for (const auto &elem: queries)
                doAndCheck(IS_POSSIBLE, PHS, FPHS, elem);
        }
    }
    
    void testTypical(unsigned int _tests, unsigned int _keys, unsigned int _type, bool _shuffle, unsigned int _min, unsigned int _max) {
        for (; _tests > 0; --_tests) {
            NPerfectHashSet::CPerfectHashSet PHS;
            std::vector<unsigned int> keys = generateTypicalUI(_keys, _type, _shuffle, _min, _max);
            try {
                PHS.init(keys);
            }
            catch (const NPerfectHashSet::CEqualKeysException &e) {
                continue;
            }
            throw CIncorrectWorkException(0);
        }
    }
}

#endif
