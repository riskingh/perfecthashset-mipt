#ifndef PerfectHashSet_FakePerfectHashSet_h
#define PerfectHashSet_FakePerfectHashSet_h

#include <set>
#include "HashFunctions.h"

/*
 * This file describes class CFakePerfectHashSet that enables to use the same
 * methods as CPerfectHashSet has.
 */

namespace NTest {
    class CFakePerfectHashSet {
    private:
        std::set<unsigned int> keys;
        std::set<unsigned int> data;
        const unsigned long long prime;
    public:
        CFakePerfectHashSet(unsigned long long _prime = NPerfectHashSet::ULL_PRIME)
        : prime(_prime) {}
        
        void init(std::vector<unsigned int> _keys) {
            keys = std::set<unsigned int>(_keys.begin(), _keys.end());
        }
        
        bool possibleKey(unsigned int _key) const {
            return keys.find(_key) != keys.end();
        }
        
        bool has(unsigned int _key) const {
            return data.find(_key) != data.end();
        }
        
        bool insert(unsigned int _key) {
            bool result = !has(_key);
            data.insert(_key);
            return result;
        }
        
        bool erase(unsigned int _key) {
            bool result = has(_key);
            data.erase(_key);
            return result;
        }

        void clear() {
            keys.clear();
            data.clear();
        }
    };
}

#endif
