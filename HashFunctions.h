#ifndef PerfectHashSet_HashFunctions_h
#define PerfectHashSet_HashFunctions_h

#include <stdio.h>
#include <vector>
#include <sstream>

/*
 * This file describes some helper functions and constants.
 *
 * ULL_PRIME is 2^32 + 15 that is minimal prime greater than 2^32.
 * MOD_64_ULL_PRIME is 2^64 % ULL_PRIME.
 *
 * getPair32 returns pair of two ULL which describes first 32 bits of number and
 * the last.
 *
 * mulULL calculates (_mul1 * _mul2) % _mod.
 *
 * hash calculates hash of _key.
 *
 * pow2 calculates _number squared.
 *
 * hasEqualNeighbours checks if there are two equal neighbours in _list. O(n)
 *
 * hasEqual checks if there are two equal in _list. O(n^2)
 */

namespace NPerfectHashSet {
    const unsigned long long ULL_PRIME = 4294967311ll;
    const unsigned long long MOD_64_ULL_PRIME = 225ll;
    
    std::pair<unsigned long long, unsigned long long> getPair32(unsigned long long _num) {
        return std::make_pair(_num >> 32ll, static_cast<unsigned long long>(static_cast<unsigned int>(_num)));
    }
    
    unsigned long long mulULL(unsigned long long _mul1, unsigned long long _mul2, unsigned long long _mod = ULL_PRIME, unsigned long long _maxULL = MOD_64_ULL_PRIME) {
        std::pair<unsigned long long, unsigned long long> mul1Pair, mul2Pair;
        mul1Pair = getPair32(_mul1);
        mul2Pair = getPair32(_mul2);
        unsigned long long result = 0ll;
        result += (((mul1Pair.first * mul2Pair.first) % _mod) * _maxULL) % _mod;
        result += ((( ((mul1Pair.first * mul2Pair.second) % _mod) + ((mul1Pair.second * mul2Pair.first) % _mod) ) % _mod) * (1ll << 32ll)) % _mod;
        result += (mul1Pair.second * mul2Pair.second) % _mod;
        return result;
    }
    
    unsigned long long hash(unsigned long long _key, std::pair<unsigned long long, unsigned long long> _params, unsigned long long _prime = ULL_PRIME, unsigned long long _maxULL = MOD_64_ULL_PRIME) {
        return (mulULL(_params.first, _key) + _params.second) % _prime;
    }
    
    inline unsigned long long pow2(const unsigned long long &_number) {
        return _number * _number;
    }


    bool hasEqualNeighbours(const std::forward_list<unsigned int> &_list, unsigned int &_key) {
        if (_list.empty()) return false;
        std::forward_list<unsigned int>::const_iterator curValueIterator = _list.begin();
        _key = *curValueIterator;
        for (++curValueIterator; curValueIterator != _list.end(); ++curValueIterator) {
            if (_key == *curValueIterator)
                return true;
            _key = *curValueIterator;
        }
        if (std::distance(_list.begin(), _list.end()) == 3ll && *_list.begin() == _key)
            return true;
        return false;
    }
    
    bool hasEqual(const std::forward_list<unsigned int> &_list, unsigned int &_key) {
        std::forward_list<unsigned int>::const_iterator firstIterator, secondIterator;
        for (firstIterator = _list.begin(); firstIterator != _list.end(); ++firstIterator) {
            secondIterator = firstIterator;
            for (++secondIterator; secondIterator != _list.end(); ++secondIterator) {
                if (*firstIterator == *secondIterator) {
                    _key = *firstIterator;
                    return true;
                }
            }
        }
        return false;
    }
    
    class CCommonException: public std::exception {
    public:
        std::string message;
        unsigned int key;
        
        CCommonException(std::string _message, unsigned int _key)
        : std::exception(), message(_message), key(_key) {}
        
        virtual const char *what() const throw() {
            std::stringstream ss;
            ss << message << " (key: " << key << ")\n";
            std::string s;
            getline(ss, s);
            return s.c_str();
        }
    };

    
    template<class _T>
    class CRandomNumber {
    private:
        std::default_random_engine generator;
        std::uniform_int_distribution<_T> distribution;
    public:
        CRandomNumber(_T _min, _T _max)
        : distribution(std::uniform_int_distribution<_T>(_min, _max)) {
        }
        _T operator ()() {
            return distribution(generator);
        }
    };
}

#endif
