#ifndef __PerfectHashSet__PerfectHashSet__
#define __PerfectHashSet__PerfectHashSet__

#include <stdio.h>
#include <vector>
#include <iostream>
#include <random>
#include <string>
#include <sstream>

#include "HashSet.h"
#include "HashFunctions.h"

/*
 * There are implementation of CPerfectHashSet and exceptions: 
 * CEqualKeysException, CImpossibleKeyException. The first one is threw when
 * there are equal keys in _keys in init method. The second one is thew when
 * one of insert, erase, has called with _key that is not in _keys in init.
 */

namespace NPerfectHashSet {

    class CEqualKeysException: public CCommonException {
    public:
        CEqualKeysException(unsigned int _key)
        : CCommonException("Equal keys in HashSet init", _key) {}
    };

    
    class CPerfectHashSet: public CCommonHashSet {
    private:
        std::vector<CHashSet> data;
//        std::vector<CHashSetLists> keys;
    public:
        CPerfectHashSet(unsigned long long _prime = ULL_PRIME)
        : CCommonHashSet(0, _prime, std::make_pair(0, 0)) {
            data.reserve(size);
//            keys.reserve(size);
        }
        
        void init(const std::vector<unsigned int> &_keys);
        
        bool possibleKey(unsigned int _key) const;
        
        // virtual methods
        bool has(unsigned int _key) const;
        bool insert(unsigned int _key);
        bool erase(unsigned int _key);
        void clear();
        
        std::vector<CHashSet>::const_iterator dataBegin() const;
        std::vector<CHashSet>::const_iterator dataEnd() const;
//        std::vector<CHashSetLists>::const_iterator keysBegin() const;
//        std::vector<CHashSetLists>::const_iterator keysEnd() const;
    };
    
    void CPerfectHashSet::init(const std::vector<unsigned int> &_keys) {
        size = _keys.size();
        CRandomNumber<unsigned long long> randomNumber0(0, prime - 1), randomNumber1(0, prime - 1);
        CHashSetLists firstLevel(_keys.size(), prime, std::make_pair(0, 0));
        unsigned int expectedSecondLevelSize, equalKey;
        do {
            firstLevel.clear();
            expectedSecondLevelSize = 0;
            firstLevel.setHashParams(std::make_pair(randomNumber1(), randomNumber0()));
            firstLevel.insertMany(_keys.begin(), _keys.end());
            for (const auto &cell: firstLevel) {
                if (hasEqualNeighbours(cell, equalKey)) throw CEqualKeysException(equalKey);
                expectedSecondLevelSize += pow2(std::distance(cell.begin(), cell.end()));
            }
        }
        while (expectedSecondLevelSize > size * 2ll);
        hashParams = firstLevel.getHashParams();
        for (const auto &cell: firstLevel) {
            if (hasEqual(cell, equalKey)) throw CEqualKeysException(equalKey);
            data.push_back(CHashSet(pow2(std::distance(cell.begin(), cell.end())), prime));
            data.back().init(cell);
        }
    }
    
    
    bool CPerfectHashSet::possibleKey(unsigned int _key) const {
        return data[index(_key)].possibleKey(_key);
    }
    
    
    // virtual methods
    bool CPerfectHashSet::has(unsigned int _key) const {
        return data[index(_key)].has(_key);
    }
    
    bool CPerfectHashSet::insert(unsigned int _key) {
        return data[index(_key)].insert(_key);
    }
    
    bool CPerfectHashSet::erase(unsigned int _key) {
        return data[index(_key)].erase(_key);
    }
    
    void CPerfectHashSet::clear() {
        for (auto &cell: data)
            cell.clear();
    }
    
    
    std::vector<CHashSet>::const_iterator CPerfectHashSet::dataBegin() const {
        return data.cbegin();
    }
    
    std::vector<CHashSet>::const_iterator CPerfectHashSet::dataEnd() const {
        return data.cend();
    }
    
//    std::vector<CHashSetLists>::const_iterator CPerfectHashSet::keysBegin() const {
//        return keys.cbegin();
//    }
//    
//    std::vector<CHashSetLists>::const_iterator CPerfectHashSet::keysEnd() const {
//        return keys.cend();
//    }
}

#endif /* defined(__PerfectHashSet__PerfectHashSet__) */
