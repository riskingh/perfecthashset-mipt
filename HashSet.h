#ifndef __PerfectHashSet__HashTable__
#define __PerfectHashSet__HashTable__

#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <forward_list>
#include <random>

#include "HashFunctions.h"

/*
 * This files describes interface IHashSet and classes CHashSet, CHashSetLists
 * which inherits IHashSet. CHashSet deals not deals with collisions.
 * CHashSetLists deals with them by saving lists of keys.
 */

namespace NPerfectHashSet {
    class IHashSet {
    public:
        IHashSet () {}
        
        virtual bool has(unsigned int _key) const = 0;
        virtual bool insert(unsigned int _key) = 0;
        virtual bool erase(unsigned int _key) = 0;
        virtual void clear() = 0;
    };
    
    class CCommonHashSet : public IHashSet {
    protected:
        unsigned long long size;
        const unsigned long long prime, maxULL;
        std::pair<unsigned long long, unsigned long long> hashParams;
        unsigned int index(const unsigned int &key) const {
            return static_cast<int>(hash(key, hashParams, prime) % size);
        }
    public:
        CCommonHashSet(unsigned long long _size, unsigned long long _prime, std::pair<unsigned long long, unsigned long long> _hashParams)
        : size(_size), prime(_prime), maxULL((((1ll << 63) % _prime) << 1) % _prime), hashParams(_hashParams) {}
        
        unsigned long long getPrime() {
            return prime;
        }
        
        unsigned int getSize() {
            return static_cast<unsigned int>(size);
        }
        
        void setHashParams(std::pair<unsigned long long, unsigned long long> _hashParams) {
            hashParams = _hashParams;
        }
        
        std::pair<unsigned long long, unsigned long long> getHashParams() {
            return hashParams;
        }
        
        void resize(unsigned long long _newSize) {
            size = _newSize;
            clear();
        }
        
        template<class _Iterator>
        bool insertMany(_Iterator _first, _Iterator _last) {
            bool allInserted = true;
            for (; _first != _last; ++_first)
                allInserted &= insert(*_first);
            return allInserted;
        }
    };
    
    class CImpossibleKeyException: public CCommonException {
    public:
        CImpossibleKeyException(unsigned int _key)
        : CCommonException("Impossible key used", _key) {}
    };
    
    class CHashSet: public CCommonHashSet {
    private:
        std::vector<bool> data;
        std::vector<unsigned int> keys;
    public:
        CHashSet(unsigned long long _size, unsigned long long _prime, std::pair<unsigned long long, unsigned long long> _hashParams = std::make_pair(0ll, 0ll))
        : CCommonHashSet(_size, _prime, _hashParams) {
            data.resize(size, false);
            keys.resize(size);
        }
        
        bool possibleKey(unsigned int _key) const {
            return _key == keys[index(_key)];
        }
        void checkPossibility(unsigned int _key) const {
            if (!possibleKey(_key)) throw CImpossibleKeyException(_key);
        }
        
        bool has(unsigned int _key) const {
            checkPossibility(_key);
            return data[index(_key)];
        }
        
        bool insert(unsigned int _key) {
            checkPossibility(_key);
            unsigned int position = index(_key);
            bool inserted = !data[position];
            data[position] = true;
            return inserted;
        }
        
        bool erase(unsigned int _key) {
            checkPossibility(_key);
            unsigned int position = index(_key);
            bool erased = data[position];
            data[position] = false;
            return erased;
        }
        
        void clear() {
            data.assign(size, false);
        }
        
        template<class KeysContainer>
        void init(const KeysContainer &_keys) {
            CRandomNumber<unsigned long long> randomNumber0(0, prime - 1), randomNumber1(1, prime - 1);
            do {
                clear();
                hashParams = std::make_pair(randomNumber1(), randomNumber0());
            }
            while (!insertMany(_keys.begin(), _keys.end()));
            for (const auto &key: _keys)
                keys[index(key)] = key;
        }
        
        std::vector<bool>::const_iterator begin() const {
            return data.cbegin();
        }
        
        std::vector<bool>::const_iterator end() const {
            return data.cend();
        }
    };
    
    
    class CHashSetLists: public CCommonHashSet {
    private:
        std::vector<std::forward_list<unsigned int>> data;
    public:
        CHashSetLists(unsigned long long _size, unsigned long long _prime, std::pair<unsigned long long, unsigned long long> _hashParams = std::make_pair(0ll, 0ll))
        : CCommonHashSet(_size, _prime, _hashParams) {
            data.resize(size);
        }
        
        bool has(unsigned int _key) const {
            int position = index(_key);
            std::forward_list<unsigned int>::const_iterator searchIterator = data[position].begin(), endIterator = data[position].end();
            for (; searchIterator != endIterator && *searchIterator != _key; ++searchIterator) {
            }
            return searchIterator != endIterator;
        }
        
        bool insert(unsigned int _key) {
            if (has(_key)) return false;
            data[index(_key)].push_front(_key);
            return true;
        }
        
        bool erase(unsigned int _key) {
            if (!has(_key)) return false;
            data[index(_key)].remove(_key);
            return true;
        }
        
        void clear() {
            data.assign(size, std::forward_list<unsigned int> ());
        }
        
        std::vector<std::forward_list<unsigned int>>::const_iterator begin() const {
            return data.begin();
        }
        std::vector<std::forward_list<unsigned int>>::const_iterator end() const {
            return data.end();
        }
    };
}

#endif /* defined(__PerfectHashSet__HashTable__) */
